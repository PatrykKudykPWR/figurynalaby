package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.Polygon;
import java.awt.geom.Area;

public class Rownoleglobok extends Figura {

	public Rownoleglobok(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		int[] Xcoor ={0,15,45,30};
		int[] Ycoor ={0,15,15,0};
		shape = new Polygon(Xcoor,Ycoor,4);
		area = new Area(shape);
		aft = new AffineTransform();
	}		
}

