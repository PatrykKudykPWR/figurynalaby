package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.Polygon;
import java.awt.geom.Area;

public class Pieciokat extends Figura {

	public Pieciokat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		int[] Xcoor ={0,12,24,18,6};
		int[] Ycoor ={8,18,8,0,0};
		shape = new Polygon(Xcoor,Ycoor,5);
		area = new Area(shape);
		aft = new AffineTransform();
	}		
}

