package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.Polygon;
import java.awt.geom.Area;

public class Trojkat extends Figura {

	public Trojkat(Graphics2D buf, int del, int w, int h) {
		super(buf, del, w, h);
		int[] Xcoor ={0,9,18};
		int[] Ycoor ={0,24,0};
		shape = new Polygon(Xcoor,Ycoor,3);
		area = new Area(shape);
		aft = new AffineTransform();
	}		
}

