/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	public int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private int width;
	private int height;
	private Color clr;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();

		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	@Override
	public void run() {
		// przesuniecie na srodek   ------- Dodana opcja losowego miejsca tworzenia na ekranie
		aft.translate((rand.nextInt(200)), rand.nextInt(150));
		area.transform(aft);
		shape = area;

		while (true) {
			// przygotowanie nastepnego kadru
			changeParams(AnimPanel.buffer, AnimPanel.width, AnimPanel.height);
			shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(area);
		aft = new AffineTransform();
		
		Rectangle2D bounds = area.getBounds();
		double cx =  bounds.getCenterX();
		double cy =  bounds.getCenterY();
		if (bounds.getWidth() > 25*(int)Math.log(width-400)  || bounds.getHeight() > 25*(int)Math.log(height-200))
		{
			if(sf > 1.0)
			sf = 1 / sf;
		}
		
		if(bounds.getHeight() < 10 || bounds.getWidth() < 10)
		{
			if(sf < 1.0)
			sf = 1 / sf;
		}
 		aft.translate(cx, cy);
		if(AnimPanel.timer.isRunning())
		{
 		aft.scale(sf, sf);
 		aft.rotate(an);
 		aft.translate(-cx, -cy);
		area.transform(aft);
		// odbicie
		if (cy-bounds.getHeight()/2 <= 0)
		{
			if(dy < 0)
			dy = -dy;
		}
		if (cy+bounds.getHeight()/2 >= height)
		{
			if(dy > 0)
			dy = -dy;
		}
		if (cx-bounds.getWidth()/2 <= 0)
		{
			if(dx < 0)
			dx = -dx;
		}	
		if (cx+bounds.getWidth()/2 >= width)
		{
			if(dx > 0)
			dx = -dx;
		}
 		aft.translate(dx, dy);
		// przeksztalcenie obiektu
 		area.transform(aft);
		}
		else
		{
			AnimPanel.timer.start();
			AnimPanel.timer.stop();
			aft.scale(1, 1);
			aft.rotate(0);
			aft.translate(-cx, -cy);
			area.transform(aft);
		}
 		return area;
 	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}

	public void changeParams(Graphics2D buf, int w, int h)
		{
			buffer = buf;
			width = w;
			height = h;
		}
	 
}
