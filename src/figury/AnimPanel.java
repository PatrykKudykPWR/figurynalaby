package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// bufor
	Image image;
	// wykreslacz ekranowy
	Graphics2D device;
	// wykreslacz bufora
	static Graphics2D buffer;

	private int delay = 70;

	static Timer timer;

	private static int numer = 0;

	public static int width,height;
	
	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		width = getWidth();
		height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
	}

	void addFig() {
		Figura fig = null;
		switch(numer%6)
		{
		case 0:
			fig =	new Kwadrat(buffer, delay, getWidth(), getHeight());
			break;
			
		case 1:
			fig = new Elipsa(buffer, delay, getWidth(), getHeight());
			break;
		
		case 2:
			fig = new Trojkat(buffer, delay, getWidth(), getHeight());
			break;
			
		case 3:
			fig = new Trapez(buffer, delay, getWidth(), getHeight());
			break;
			
		case 4:
			fig = new Rownoleglobok(buffer, delay, getWidth(), getHeight());
			break;
		case 5:
			fig = new Pieciokat(buffer, delay, getWidth(), getHeight());
			break;
		}
		numer++;
		timer.addActionListener(fig);
		new Thread(fig).start();
	}

	void animate() {
		if (timer.isRunning()) {
			timer.stop();
		} else {
			timer.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}
